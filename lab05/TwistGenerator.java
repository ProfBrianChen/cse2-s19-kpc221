// Kirk Cobb
// 3/1/2019
// CSE 002

import java.util.Scanner;   //imports scanner tool
   public class TwistGenerator {
    public static void main(String[] args){
      Scanner myScanner = new Scanner(System.in);
      System.out.println("Enter a positive integer: ");
      int value = 0;                   //loads variable to = 0;
      boolean flag = true;             //loads variable
         if (myScanner.hasNextInt()){  //makes sure the integer has been entered
         value = myScanner.nextInt();  //value is an integer
         if ( value < 0){              //checks if the value is negative
          flag = false;                //if flag is negative, makes it false
          }
        }
         else {
          flag = false;                //if flag is a non-integer, makes it false
          }
      
      while (!flag){ 
        myScanner.nextLine();          //clears scanner
        System.out.println("Enter a positive integer: ");
        if (myScanner.hasNextInt()){   //checks if user entered integer
          value = myScanner.nextInt(); //makes value equal to the integer entered
          if (value < 0){
            flag = false;              //if flag is negative, makes it false
          }
          else {
            flag = true; //makes flag true for a positive integer
          }
        }   //ends if statement
      }     //ends while loop 
    
        int leftover = value % 3;
        int twist = value / 3;
    
          for(int i = 0; i < twist; i++){ //makes a for loop
            System.out.print("\\ /");
          }
          if (leftover == 1){       //prints if leftover = 1
            System.out.print("\\");
          }
          if (leftover == 2){       //prints if leftover = 2
            System.out.print("\\ ");
          }
          System.out.print("\n");

          for(int i = 0; i < twist; i++){ //makes a loop for the # of twists
            System.out.print(" X ");
          }
          if (leftover == 1){       //prints if leftover = 1
            System.out.print(" ");
          }
          if (leftover == 2){       //prints if leftover = 2
            System.out.print(" X");
          }
          System.out.print("\n");


          for(int i = 0; i < twist; i++){ //makes loop for the # of twists
            System.out.print("/ \\");
          }
          if (leftover == 1){       //prints if leftover = 1
            System.out.print("/");
          }
          if (leftover == 2){       //prints if leftover = 2
            System.out.print("/ ");
          }
          System.out.print("\n");
    
  } // ends method
} // ends class