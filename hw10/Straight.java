//Kirk Cobb
//CSE 002
//5/1/2019

import java.util.Scanner;   //imports scanner
import java.util.Arrays;    //imports arrays
import java.util.Random;
public class Straight{
  
  public static int[] getHand(){// creates a sorted hand of cards
    int[] hand = new int[52];// make an intial hand in order
for(int i=0;i<=51;i++){// input intial values into hand
  hand[i]=i;
  }

Random rgen = new Random();  // Random number generator   
  for (int i=0; i<hand.length; i++) {// shuffles initially generated hand
      int randomPosition = rgen.nextInt(hand.length);
      int temp = hand[i];
      hand[i] = hand[randomPosition];
      hand[randomPosition] = temp;
  }
  return hand;// Returns shuffled hand
  }
  
  public static int[] draw(int []deck){// Method to draw first 5 cards
    int[] drawHand = new int[5];// creates array to hold drawn deck
    for (int i=0; i<drawHand.length; i++){//draws 5 cards
      drawHand[i]=deck[i];//fills draw hand
    }
    return drawHand;// returns drawn hand
  }
  
  public static int search(int []deck1, int k){// Method searches for a straight
    if(k>5 || k<0){
      System.out.println("K not in range of 1-5");
    }
   int[] sorted = new int[5];
   for(int i = 0; i < sorted.length; i++) {// fill in a deck
     sorted[i] = (deck1[i] %13); 
   }
   Arrays.sort(sorted);
   for(int i = 0; i <= sorted.length; i++) {
      if (i==k){
       return (sorted[i]);// Returns the kth lowest value
      }
    } 
   
        return -1;// Retuns -1 if not found
      
    } 
  
  
  public static int straight(){// Method checks if there is a straight
    int count = 0;// keeps track of which cards are different by 1
    int [] hand = getHand();// gets hand
                     
    int draw [] = draw(hand);// draws hand
                     

     for(int x = 0; x<4; x++){
       int w = search(draw,x);// gets Kth lowest
       int k = search(draw,x+1);// gets Kth lowest + 1
 
     if((k-w)==1){// if true, cards are seperated by 1 
       count++;
     }
     }
//     System.out.println("The count is " + count);
     if(count == 4){// if count == 4 a straight exists
       return 1;// Returns 1 to signify a straight exists
     }
     return -1;// no straight exists
  }
  
  
  public static void main(String [] args) { //main method
  int numberFound = 0;// keeps track of how many straights are found
  for( int i = 0; i<=1000000;i++){// checks 1 million pairs of hands
  int found = straight();// checks hand
  if(found == 1){// if find() returns 1, a straight exists
    numberFound++;// adds one straight to the total 
  }
  }
  System.out.println("I found " + numberFound + " straights");// displays results
    
   }
  }
