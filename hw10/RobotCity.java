//Kirk Cobb
//CSE 002
//4/30/2019
import java.util.Scanner;  //import scanner
import java.util.Arrays;//import arrays
import java.util.Random;//needed for random number generation
public class RobotCity{//class
  
public static int[][] buildCity(){// method to generate intial city
int x= (int)(Math.random()*6)+10;// x number of blocks 
int y= (int)(Math.random()*6)+10;// y number of blocks
int[][] cityArray = new int[x][y];// declare array to hold the city blocks
for(int i=0;i<x;i++){

for(int j=0;j<y;j++){
int r= (int)(Math.random()*900)+100;// assign a value to each block
cityArray[i] [j] =r;
}
}

return cityArray;// return city 
}
  
public static void display(int cityArray[][]){
int x= cityArray.length;// get dimensoins of city 
int y=cityArray[1].length;
x--;

for(int i=0;i<x;i++){
for(int j=0;j<y;j++){
  System.out.printf("%8s", cityArray[(x-i)][(j)]);// display each element of the city formatted
}
System.out.println("");


}
return;
  
}

public static void invade(int k, int cityArray[][]){
int x= cityArray.length;// get dimensions
int y=cityArray[1].length;
for(int i=0;i<=k;i++){// this loop chooses a block to invade
int x_cord= (int)(Math.random()*x);
int y_cord= (int)(Math.random()*y);
if(cityArray[x_cord][y_cord]>0){// check to see if block was already invaded
  cityArray[x_cord][y_cord]=cityArray[x_cord][y_cord]*(-1);// invade block 
}
else{
  i--;// run loop anohther time because the chosen block was already invaded 
}
}
}
  
public static void update(int cityArray[][]){// this loop invades the tile to the east of the robot 
int x= cityArray.length;
int y=cityArray[1].length;
x--;
y--;
for(int i=0;i<x;i++) {
  for(int j=0;j<y;j++){
    if(cityArray[i][j]<0&&(j<y)){// check if block has robot 
      if(cityArray[i][(j+1)]>0){ //check if block has robot 
        cityArray[i][(j+1)]=cityArray[i][(j+1)]*(-1);
        j++;// skip to next block 
      }
    }
  }
} 
  
  
}
  
  
  
  
public static void main(String [] args) { //main method
Scanner myscanner = new Scanner(System.in); // make a scanner 
int[][] cityArray = buildCity();// create array to hold city 

 display(cityArray);// display array 
 //int k=myscanner.nextInt();// store numbe of robots 
 int k = 1 + (int)(Math.random() * ((10 - 1) + 1));// generate a random number of robots from 1 to 10
 invade(k,cityArray);// place robots 
 System.out.println("");// skip lines to make output look nice 
 System.out.println("");
 System.out.println("");
 display(cityArray);// display updated array 
 for(int i=0;i<5;i++){// update and display the array 5 for times
 System.out.println("");// put spaces inbetween updated cities
 System.out.println("");
 System.out.println("");
 update(cityArray);// move robots east 
 display(cityArray);// print updated city 

 }
 
 
 
  
}
}