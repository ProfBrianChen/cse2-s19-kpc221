//Kirk Cobb
//4/16/19
//CSE 002

import java.util.Scanner;

public class ArrayGames {

    public static int [] generate() {            //adds generate() method
        int random = (int)(Math.random() * 10 + 1) + 10; 
        int [] myArray = new int[random];            //creates array from set of random numbers
        for (int i = 0; i < myArray.length -1; i ++) {    //loops until array reaches proper length
            myArray[i] = i; 
        }    
        return myArray;
    }

    public static void print(int [] myArray) {            //adds spacing between numbers of array
        for (int i =0; i < myArray.length -1; i++) {
            System.out.print(myArray[i] + " ");
        }
        System.out.println();
    }

    public static int [] insert(int[] a, int [] b) {     //insert method for array
        int [] output = new int[a.length + b.length];
        int index = (int)(Math.random() * a.length);
        for (int i =0; i < output.length-1; i++) {
            if (i < index) {
                output[i] = a[i];
            }
            else if (i >= index && i <= index + b.length-1) {
                output[i] = b[i - index];
            }
            else {
                output[i] = a[i- b.length];
            }
        }
        return output;
    }   

    public static int[] shorten(int[] a, int index) {     //shorten method for array
        if (index > a.length -1) {
            return a;
        }
        int [] output = new int [a.length-1];
        for (int i = 0; i < output.length -1; i++) {
            if (i < index) {
                output[i] = a[i];
            }
            else if (i >= index) {
                output[i] = a[i+1];
            }
        }
        return output;
    }

    public static void main (String [] args) {
        Scanner input = new Scanner(System.in);        //imports scanner for user prompt

        System.out.print("Would you like to use 1. Insert or 2. Shorten? type 1 or 2: ");
        int choice = input.nextInt();                    //asks for input to determine method
        
        System.out.println("User input: " + choice);   //assigns 1 value to insert method
        if (choice == 1) {
           System.out.println("Output: ");               
           System.out.print("Array: ");
            int [] a = generate();
            print(a);
            System.out.print("Array 2 : ");
            int [] b = generate();
            print(b);
            
           print(insert(a, b));
        }
        else if (choice == 2) {                        //assigns 2 value to shorten method
            System.out.println("Array: ");
            int [] a = generate();
            print(a);
            System.out.print("Where would you like to shorten the array?");
            int index = input.nextInt();
            System.out.println("Input: " + index);        //prints user input as well as output array
            System.out.println("Output: ");
            print (shorten(a, index));
         }
        else {
            System.out.println("Your input isnt correct :(");        //prints error if it falls outside range
        }

    }
}