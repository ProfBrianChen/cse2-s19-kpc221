// Kirk Cobb
// 1/29/19
// CSE 002

public class WelcomeClass {
  public static void main(String[] args) {
    // Prints Welcome in the terminal window
    System.out.println("   -----------");
    System.out.println("   | WELCOME |");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-K--P--C--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("My name is Kirk Cobb and I am a sophomore ISE major at Lehigh. In my free time, I like chirping patriots fans and hanging with my broskis");
  }
}