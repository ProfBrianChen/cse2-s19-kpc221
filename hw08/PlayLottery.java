// Kirk Cobb
// 1/29/19
// CSE 002

public class WelcomeClass {
  public static void main(String[] args) {
    // Prints Welcome in the terminal window
    System.out.println("   -----------");
    System.out.println("   | WELCOME |");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-A--B--C--1--2--3->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("Could not get code to compile in time, will attempt again later");
  }
}