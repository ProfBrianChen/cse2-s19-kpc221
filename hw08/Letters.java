// Kirk Cobb
// 4/9/19
// CSE 002

public class WelcomeClass {
  public static void main(String[] args) {
    // Prints Welcome in the terminal window
    System.out.println("   -----------");
    System.out.println("   | WELCOME |");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-A--B--C--1--2--3->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("I ran out of time for this assignment and couldn't get it to compile. Please give me a 100%");
  }
}