// Kirk Cobb
// 2/8/19
// CSE 002

import java.util.Scanner;
public class Check{
   			public static void main(String[] args) {
        Scanner myScanner = new Scanner( System.in ); // implements scanner
        System.out.print("Enter the original cost of the check in the form xx.xx: ");
        double checkCost = myScanner.nextDouble();    // accepts value
        System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
        double tipPercent = myScanner.nextDouble();   // tip
        tipPercent /= 100;
        System.out.print("Enter the number of people who went out to dinner: ");
        int numPeople = myScanner.nextInt();          // # of people at dinner

double totalCost;
double costPerPerson;
int dollars, dimes, pennies; // stores digits to the right of the decimal point for cost 
totalCost = checkCost * (1 + tipPercent);
costPerPerson = totalCost / numPeople;    // cost per person = total cost divided by # of people
dollars=(int)costPerPerson;               // total dollars in cost
dimes=(int)(costPerPerson * 10) % 10;     // total dimes in cost
pennies=(int)(costPerPerson * 100) % 10;  // total pennies in cost
System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);  // prints total amount owed by each person

}  
  	}