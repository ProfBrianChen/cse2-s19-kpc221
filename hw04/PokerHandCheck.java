// Kirk Cobb
// 2/19/2019
// CSE 002

import java.util.Scanner; //imports Scanner 
import java.util.concurrent.ThreadLocalRandom;
public class PokerHandCheck //creates the class
{
  public static void main (String [] args) //main method
  {
    
    int c1 = 0; // initializes c which will hold the card numbers
    String s1 = "";
    int c2 = 0;
    String s2 = "";
    int c3 = 0;
    String s3 = "";
    int c4 = 0;
    String s4 = "";
    int c5 = 0;
    String s5 = "";
    
    int rand = 0;
    
    int pair = 0; //creates counter for one pair
    int twoPair = 0;
    int three = 0;
    int threeDetected = 0; //stores the number on the three cards if three of a kind is found
    
 
    
    rand = (ThreadLocalRandom.current().nextInt(1, 52 + 1));
    c1 = rand%13;
    if (c1 == 0) {
      c1 = 13;
    }
    if (1<= rand/13 && rand/13 <=13) {
      s1 = "Diamonds";
    }
    if (14<= rand/13 && rand/13 <=26) {
      s1 = "Hearts";
    }
    if (27<= rand/13 && rand/13 <=39) {
      s1 = "Diamonds";
    }
    if (40<= rand/13 && rand/13 <=52) {
      s1 = "Diamonds";
    }

    rand = (ThreadLocalRandom.current().nextInt(1, 52 + 1));
    c2 = rand%13;
    if (c2 == 0) {
      c2 = 13;
    }
    if (1<= rand/13 && rand/13 <=13) {
      s2 = "Diamonds";
    }
    if (14<= rand/13 && rand/13 <=26) {
      s2 = "Hearts";
    }
    if (27<= rand/13 && rand/13 <=39) {
      s2 = "Diamonds";
    }
    if (40<= rand/13 && rand/13 <=52) {
      s2 = "Diamonds";
    }
    rand = (ThreadLocalRandom.current().nextInt(1, 52 + 1));
    c3 = rand%13;
    if (c3 == 0) {
      c3 = 13;
    }
    if (1<= rand/13 && rand/13 <=13) {
      s3 = "Diamonds";
    }
    if (14<= rand/13 && rand/13 <=26) {
      s3 = "Hearts";
    }
    if (27<= rand/13 && rand/13 <=39) {
      s3 = "Diamonds";
    }
    if (40<= rand/13 && rand/13 <=52) {
      s3 = "Diamonds";
    }

    rand = (ThreadLocalRandom.current().nextInt(1, 52 + 1));
    c4 = rand%13;
    if (c4 == 0) {
      c4 = 13;
    }
    if (1<= rand/13 && rand/13 <=13) {
      s4 = "Diamonds";
    }
    if (14<= rand/13 && rand/13 <=26) {
      s4 = "Hearts";
    }
    if (27<= rand/13 && rand/13 <=39) {
      s4 = "Diamonds";
    }
    if (40<= rand/13 && rand/13 <=52) {
      s4 = "Diamonds";
    }

    rand = (ThreadLocalRandom.current().nextInt(1, 52 + 1));
    c5 = rand%13;
    if (c5 == 0) {
      c5 = 13;
    }
    if (1<= rand/13 && rand/13 <=13) {
      s5 = "Diamonds";
    }
    if (14<= rand/13 && rand/13 <=26) {
      s5 = "Hearts";
    }
    if (27<= rand/13 && rand/13 <=39) {
      s5 = "Diamonds";
    }
    if (40<= rand/13 && rand/13 <=52) {
      s5 = "Diamonds";
      
      //three of a kind
      if ( (c1 == c2) && (c1 == c3)) //if cards 1, 2, 3 are the same
      {
        three++;
        threeDetected = c1;
      }
      if ( (c1 == c2) && (c1 == c4)) //if cards 1, 2, 4 are the same
      {
        three++;
        threeDetected = c1;
      }
      if ( (c1 == c2) && (c1 == c5)) //if cards 1, 2, 5 are the same
      {
        three++;
        threeDetected = c1;
      }
      if ( (c1 == c3) && (c1 == c4)) //if cards 1, 3, 4 are the same
      {
        three++;
        threeDetected = c1;
      }
      if ( (c1 == c3) && (c1 == c5)) //if cards 1, 3, 5 are the same
      {
        three++;
        threeDetected = c1;
      }
      if ( (c1 == c4) && (c1 == c5)) //if cards 1, 4, 5 are the same
      {
        three++;
        threeDetected = c1;
      }
      if ( (c2 == c3) && (c2 == c4)) //if cards 2, 3, 4 are the same
      {
        three++;
        threeDetected = c2;
      }
      if ( (c2 == c3) && (c2 == c5)) //if cards 2, 3, 5 are the same
      {
        three++;
        threeDetected = c2;
      }
      if ( (c2 == c3) && (c2 == c4)) //if cards 2, 4, 5 are the same
      {
        three++;
        threeDetected = c2;
      }
      if ( (c3 == c4) && (c3 == c5)) //if cards 3, 4, 5 are the same
      {
        three++;
        threeDetected = c3;
      }
      
      //pair and two pair
      if ( (c1 == c2) && (c1 != threeDetected)) //if cards 1, 2 are the same
      {
        if (((c3 == c4 || c3 == c5) && c3 != threeDetected) || ((c4 == c5) && (c4 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c1 == c3) && (c1 != threeDetected)) //if cards 1, 3 are the same
      {
        if (((c2 == c4 || c2 == c5) && c2 != threeDetected) || ((c4 == c5) && (c4 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c1 == c4) && (c1 != threeDetected)) //if cards 1, 4 are the same
      {
        if (((c2 == c3 || c2 == c5) && c2 != threeDetected) || ((c3 == c5) && (c3 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c1 == c5) && (c1 != threeDetected)) //if cards 1, 5 are the same
      {
        if (((c2 == c3 || c2 == c4) && c2 != threeDetected) || ((c3 == c4) && (c3 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c2 == c3) && (c2 != threeDetected)) //if cards 2, 3 are the same
      {
        if (((c1 == c4 || c1 == c5) && c1 != threeDetected) || ((c4 == c5) && (c4 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c2 == c4) && (c2 != threeDetected)) //if cards 2, 4 are the same
      {
        if (((c1 == c3 || c1 == c5) && c1 != threeDetected) || ((c3 == c5) && (c3 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c2 == c5) && (c2 != threeDetected)) //if cards 2, 5 are the same
      {
        if (((c1 == c3 || c1 == c4) && c1 != threeDetected) || ((c3 == c5) && (c3 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c3 == c4) && (c3 != threeDetected)) //if cards 3, 4 are the same
      {
        if (((c1 == c2 || c1 == c5) && c1 != threeDetected) || ((c2 == c5) && (c2 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c3 == c5) && (c3 != threeDetected)) //if cards 3, 5 are the same
      {
        if (((c1 == c2 || c1 == c4) && c1 != threeDetected) || ((c2 == c4) && (c2 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
      else if ( (c4 == c5) && (c4 != threeDetected)) //if cards 4, 5 are the same
      {
        if (((c1 == c2 || c1 == c3) && c1 != threeDetected) || ((c2 == c3) && (c2 != threeDetected))) //if there is another pair that's not three of a kind
        {
          twoPair++;
        }
        else
        {
          pair++;
        }
      }
    }
    System.out.println("Your random cards were"); //prints the output
    System.out.println("The "+c1+" of "+s1);
    System.out.println("The "+c2+" of "+s2);
    System.out.println("The "+c3+" of "+s3);
    System.out.println("The "+c4+" of "+s4);
    System.out.println("The "+c5+" of "+s5);
    if (pair == 1)
    {
      System.out.println("You have a pair!");
    }
    else if (twoPair == 1)
    {
      System.out.println("You have two pair!");
    }
    else if (three == 1)
    {
      System.out.println("You have three of a kind!");
    }
    else {
      System.out.println("You have a high card!");
    }
  }
}