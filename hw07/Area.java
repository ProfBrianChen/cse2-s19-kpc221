//Kirk Cobb
//3/27/19
//CSE 002

import java.util.*;
public class Main{
   public static void main(String args[]){
       String shape;
       int i = 0;
       double length,breadth,radius,base,height;
       System.out.print("Enter a shape: ");
       Scanner sc = new Scanner(System.in);
       do{
           i = 0;
           shape = sc.next();
           if(shape.equals("rectangle")){
               System.out.print("Enter length: ");
               length = sc.nextDouble();
               System.out.print("Enter width: ");
               breadth = sc.nextDouble();
               if(length >= 0 && breadth >= 0){
                   Main.rectangle(length,breadth);
               }else{
                   System.out.println("\nInvalid input, try again");
               }
           }else if(shape.equals("triangle")){
               System.out.print("Enter height: ");
               height = sc.nextDouble();
               System.out.print("Enter base: ");
               base = sc.nextDouble();
               if(height >= 0 && base >= 0){
                   Main.triangle(height,base);
               }else{
                   System.out.println("\nInvalid input, try again");
               }
           }else if(shape.equals("circle")){
               System.out.print("Enter radius: ");
               radius = sc.nextDouble();
               if(radius >= 0 ){
                   Main.circle(radius);
               }else{
                   System.out.println("\nInvalid input, try again");
               }
           }else{
               System.out.print("\nEnter a valid shape: ");
               i = 1;
           }
       }while (i != 0);
   }
   public static void rectangle(double length,double width){
       System.out.println("\nThe area of the rectangle is " + length*width);
   }
   public static void triangle(double height,double base){
       System.out.println("\nThe area of the triangle is " + 0.5*height*base);
   }
   public static void circle(double radius){
       System.out.println("\nThe area of the circle is " + 3.14*radius*radius);
   }
}