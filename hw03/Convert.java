// Kirk Cobb
// 2/12/2019
// CSE 002

import java.util.Scanner;               // searches for scanner tool
public class Convert {
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);                 // express scanner
    System.out.print("Enter the distance in meters: ");     // prompts user formeter input
    
       double Meters = myScanner.nextDouble();      // calculation for inches
       double Inches;
   
      Inches = Meters * 39.3701;
    System.out.println(Meters + " meters is " + Inches + " inches");    // prints the original meters and conversion to inches
    
  }
}